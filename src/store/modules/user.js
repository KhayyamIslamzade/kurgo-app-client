import { getInfo } from '@/api/user'
import { login } from '@/api/auth'
import { getToken, setToken, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router'
const state = {
  token: getToken(),
  name: '',
  id: '',
  email: '',
  isDetailCompleted: false,
  directivePermissions: [],
  avatar: require('@/assets/avatar.png'),
  introduction: '',
  roles: []
}

const mutations = {
  SET_TOKEN: (state, payload) => {
    state.token = payload
  },
  SET_DIRECTIVE_PERMISSIONS: (state, payload) => {
    state.directivePermissions = payload
  },
  SET_INTRODUCTION: (state, payload) => {
    state.introduction = payload
  },
  SET_NAME: (state, payload) => {
    state.name = payload
  },
  SET_EMAIL: (state, payload) => {
    state.email = payload
  },
  SET_IS_DETAIL_COMPLETED: (state, payload) => {
    state.isDetailCompleted = payload
  },
  SET_ID: (state, payload) => {
    state.id = payload
  },
  SET_AVATAR: (state, payload) => {
    state.avatar = payload
  },
  SET_ROLES: (state, payload) => {
    state.roles = payload
  }
}

function mapRoleObj(roles) {
  const obj = roles.map(role => {
    const name = role.name.toLowerCase()
    let permissions = []
    if (role.permissions && role.permissions.length > 0) {
      permissions = role.permissions.map(item => {
        return `${item.category.label}_${item.permission.label}`.toLowerCase()
      })
    }
    return { name, permissions }
  })

  return obj
}
function mapPermissionObj(permissions) {
  if (permissions && permissions.length > 0) {
    return permissions.map(item => {
      return item.label.toLowerCase()
    })
  } else return []
}

const actions = {
  // user login
  login({ commit }, data) {
    const { emailOrUsername, password } = data

    return new Promise((resolve, reject) => {
      login({ emailOrUsername: emailOrUsername.trim(), password: password })
        .then(response => {
          const { token } = response

          commit('SET_TOKEN', token)
          setToken(token)
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo()
        .then(response => {
          if (!response) {
            reject('Verification failed, please Login again.')
          }

          let { roles } = response

          const {
            directivePermissions,
            userName,
            email,
            id,
            isDetailCompleted
          } = response

          // roles must be a non-empty array
          if (!roles || roles.length <= 0) {
            roles = [{ name: 'none', permissions: [] }]
          }

          if (!roles || roles.length <= 0) {
            reject('getInfo: roles must be a non-null array!')
          }

          const mappedRoles = mapRoleObj(roles)
          const mappedDirectivePermissions = mapPermissionObj(
            directivePermissions
          )

          commit('SET_ROLES', mappedRoles)
          commit('SET_DIRECTIVE_PERMISSIONS', mappedDirectivePermissions)
          commit('SET_NAME', userName)
          commit('SET_EMAIL', email)
          commit('SET_IS_DETAIL_COMPLETED', isDetailCompleted)
          commit('SET_ID', id)
          resolve({
            roles: mappedRoles,
            directivePermissions: mappedDirectivePermissions,
            userName,
            isDetailCompleted,
            id
          })
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // user logout
  logout({ commit, state, dispatch }) {
    commit('SET_TOKEN', '')
    commit('SET_ROLES', [])
    commit('SET_NAME', '')
    commit('SET_DIRECTIVE_PERMISSIONS', [])
    removeToken()
    resetRouter()

    dispatch('tagsView/delAllViews', null, { root: true })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  },

  // dynamically modify permissions
  changeRoles({ commit, dispatch }, role) {
    return new Promise(async resolve => {
      const token = role + '-token'

      commit('SET_TOKEN', token)
      setToken(token)

      const { roles } = await dispatch('getInfo')

      resetRouter()

      // generate accessible routes map based on roles
      const accessRoutes = await dispatch('permission/generateRoutes', roles, {
        root: true
      })

      // dynamically add accessible routes
      router.addRoutes(accessRoutes)

      // reset visited views and cached views
      dispatch('tagsView/delAllViews', null, { root: true })

      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
