import { asyncRoutes, constantRoutes } from '@/router'
import {
  getRolePermissions,
  hasRole,
  hasPermission,
  hasDirectivePermission
} from '@/helpers/permission'

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(
  routes,
  roles,
  permissions,
  directivePermissions
) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    const canAccess =
      hasRole(roles, tmp) ||
      hasPermission(permissions, tmp) ||
      hasDirectivePermission(directivePermissions, tmp)

    if (canAccess) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(
          tmp.children,
          roles,
          permissions,
          directivePermissions
        )
      }
      // if children defined but dont have anything(thats mean its a empty group) then dont add
      if (!(tmp.children && tmp.children.length <= 0)) res.push(tmp)
    }
  })

  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }, data) {
    const { roles, directivePermissions } = data
    const permissions = getRolePermissions(roles)
    return new Promise(resolve => {
      let accessedRoutes = []

      if (roles.includes('admin') || directivePermissions.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        accessedRoutes = filterAsyncRoutes(
          asyncRoutes,
          roles,
          permissions,
          directivePermissions
        )
      }
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
