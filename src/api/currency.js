import request from '@/utils/request'
import URLS from '@/helpers/urls'

export function getCurrencies() {
  return request({
    url: URLS.CURRENCY_URLS.GET_ALL_URL,
    method: 'get'
  })
}
export function addCurrency(data) {
  return request({ url: URLS.CURRENCY_URLS.ADD_URL, method: 'post', data })
}
export function getCurrency(id) {
  return request({
    url: URLS.CURRENCY_URLS.GET_URL + '/' + id,
    method: 'get'
  })
}
export function editCurrency(data) {
  return request({ url: URLS.CURRENCY_URLS.EDIT_URL, method: 'post', data })
}

export function deleteCurrency(id) {
  return request({
    url: URLS.CURRENCY_URLS.DELETE_URL + '/' + id,
    method: 'post'
  })
}
