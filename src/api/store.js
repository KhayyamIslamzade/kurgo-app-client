import request from '@/utils/request'
import URLS from '@/helpers/urls'

export function getStores() {
  return request({
    url: URLS.STORE_URLS.GET_ALL_URL,
    method: 'get'
  })
}
export function addStore(data) {
  return request({ url: URLS.STORE_URLS.ADD_URL, method: 'post', data })
}
export function getStore(id) {
  return request({
    url: URLS.STORE_URLS.GET_URL + '/' + id,
    method: 'get'
  })
}
export function editStore(data) {
  return request({
    url: URLS.STORE_URLS.EDIT_URL,
    method: 'post',
    data
  })
}

export function deleteStore(id) {
  return request({
    url: URLS.STORE_URLS.DELETE_URL + '/' + id,
    method: 'post'
  })
}
