import request from '@/utils/request'
import URLS from '@/helpers/urls'

export function getCategories(params) {
  return request({
    url: URLS.CATEGORY_URLS.GET_ALL_URL,
    method: 'get',
    params
  })
}
export function addCategory(data) {
  return request({ url: URLS.CATEGORY_URLS.ADD_URL, method: 'post', data })
}
export function getCategory(id) {
  return request({
    url: URLS.CATEGORY_URLS.GET_URL + '/' + id,
    method: 'get'
  })
}
export function editCategory(data) {
  return request({ url: URLS.CATEGORY_URLS.EDIT_URL, method: 'post', data })
}

export function deleteCategory(id) {
  return request({
    url: URLS.CATEGORY_URLS.DELETE_URL + '/' + id,
    method: 'post'
  })
}
