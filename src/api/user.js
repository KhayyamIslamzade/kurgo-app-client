import request from "@/utils/request";
import URLS from "@/helpers/urls";

export function getInfo() {
  return request({
    url: URLS.USER_URLS.GET_INFO_URL,
    method: "get"
  });
}
export function getUsers() {
  return request({
    url: URLS.USER_URLS.GET_ALL_URL,
    method: "get"
  });
}

export function getUser(id) {
  return request({
    url: URLS.USER_URLS.GET_URL + "/" + id,
    method: "get"
  });
}
export function getDetail(id) {
  return request({
    url: URLS.USER_URLS.GET_DETAIL_URL + "/" + id,
    method: "get"
  });
}
export function addUser(data) {
  return request({ url: URLS.USER_URLS.ADD_URL, method: "post", data });
}

export function editUser(data) {
  return request({ url: URLS.USER_URLS.EDIT_URL, method: "post", data });
}
export function updateDetail(data) {
  return request({
    url: URLS.USER_URLS.UPDATE_DETAIL_URL,
    method: "post",
    data
  });
}
export function changePassword(data) {
  return request({
    url: URLS.USER_URLS.CHANGE_PASSWORD_URL,
    method: "post",
    data
  });
}
export function deleteUser(id) {
  return request({
    url: URLS.USER_URLS.DELETE_URL + "/" + id,
    method: "post"
  });
}
