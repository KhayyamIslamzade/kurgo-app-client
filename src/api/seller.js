import request from '@/utils/request'
import URLS from '@/helpers/urls'

export function getSellers(params) {
  return request({
    url: URLS.SELLER_URLS.GET_ALL_URL,
    method: 'get',
    params
  })
}
export function addSeller(data) {
  return request({ url: URLS.SELLER_URLS.ADD_URL, method: 'post', data })
}
export function getSeller(id) {
  return request({
    url: URLS.SELLER_URLS.GET_URL + '/' + id,
    method: 'get'
  })
}
export function editSeller(data) {
  return request({ url: URLS.SELLER_URLS.EDIT_URL, method: 'post', data })
}

export function deleteSeller(id) {
  return request({
    url: URLS.SELLER_URLS.DELETE_URL + '/' + id,
    method: 'post'
  })
}
