import request from '@/utils/request'
import URLS from '@/helpers/urls'

export function getRoles() {
  return request({
    url: URLS.ROLE_URLS.GET_ALL_URL,
    method: 'Get'
  })
}
export function getRole(id) {
  return request({
    url: URLS.ROLE_URLS.GET_URL + '/' + id,
    method: 'get'
  })
}
export function addRole(data) {
  return request({
    url: URLS.ROLE_URLS.ADD_URL,
    method: 'post',
    data
  })
}

export function editRole(data) {
  return request({
    url: URLS.ROLE_URLS.EDIT_URL,
    method: 'post',
    data
  })
}

export function deleteRole(id) {
  return request({
    url: URLS.ROLE_URLS.DELETE_URL + '/' + id,
    method: 'post'
  })
}
