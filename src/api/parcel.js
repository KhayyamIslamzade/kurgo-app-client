import request from '@/utils/request'
import URLS from '@/helpers/urls'

export function changeStatus(data) {
  return request({
    url: URLS.PARCEL_URLS.CHANGE_STATUS_URL,
    method: 'post',
    data
  })
}
export function setOptions(data) {
  return request({
    url: URLS.PARCEL_URLS.SET_OPTIONS_URL,
    method: 'post',
    data
  })
}
export function getParcels(params) {
  return request({
    url: URLS.PARCEL_URLS.GET_ALL_URL,
    method: 'get',
    params
  })
}
export function addParcel(data) {
  return request({ url: URLS.PARCEL_URLS.ADD_URL, method: 'post', data })
}
export function getParcel(id) {
  return request({
    url: URLS.PARCEL_URLS.GET_URL + '/' + id,
    method: 'get'
  })
}
export function editParcel(data) {
  return request({ url: URLS.PARCEL_URLS.EDIT_URL, method: 'post', data })
}

export function deleteParcel(id) {
  return request({
    url: URLS.PARCEL_URLS.DELETE_URL + '/' + id,
    method: 'post'
  })
}
