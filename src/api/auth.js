import request from '@/utils/request'
import URLS from '@/helpers/urls'

export function login(data) {
  return request({ url: URLS.AUTHORIZE_URLS.LOGIN_URL, method: 'post', data })
}
export function register(data) {
  return request({
    url: URLS.AUTHORIZE_URLS.REGISTER_URL,
    method: 'post',
    data
  })
}
export function confirmEmail(userId, token) {
  const params = { userId, token }
  return request({
    returnFullResponse: true,
    popupErrorMessage: false,
    url: URLS.AUTHORIZE_URLS.CONFIRM_EMAIL,
    method: 'get',
    params
  })
}
export function forgotPassword(email) {
  const params = { email }
  return request({
    returnFullResponse: true,
    popupErrorMessage: false,
    url: URLS.AUTHORIZE_URLS.FORGOT_PASSWORD,
    method: 'get',
    params
  })
}
export function checkPasswordReset(userId) {
  const params = { userId }
  return request({
    returnFullResponse: true,
    popupErrorMessage: false,
    url: URLS.AUTHORIZE_URLS.CHECK_PASSWORD_RESET,
    method: 'get',
    params
  })
}
export function passwordReset(data) {
  return request({
    returnFullResponse: true,
    popupErrorMessage: false,
    url: URLS.AUTHORIZE_URLS.PASSWORD_RESET,
    method: 'post',
    data
  })
}
