import request from '@/utils/request'
import URLS from '@/helpers/urls'

export function getPermissions() {
  return request({
    url: URLS.PERMISSION_URLS.GET_ALL_URL,
    method: 'Get'
  })
}

export function getDirectivePermissions() {
  return request({
    url: URLS.PERMISSION_URLS.GET_ALL_DIRECTIVE_PERMISSIONS_URL,
    method: 'Get'
  })
}
