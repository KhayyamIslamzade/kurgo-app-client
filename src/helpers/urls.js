const AUTHORIZE_URLS = {
  LOGIN_URL: 'Account/Login',
  REGISTER_URL: 'Account/Register',
  CONFIRM_EMAIL: 'Account/ConfirmEmail',
  FORGOT_PASSWORD: 'Account/ForgotPassword',
  CHECK_PASSWORD_RESET: 'Account/CheckPasswordReset',
  PASSWORD_RESET: 'Account/PasswordReset'
}
const USER_URLS = {
  GET_ALL_URL: '/User/GetAll',
  GET_ALL_WITH_ROLE_PERMISSION_URL: '/User/GetAllWithRolePermission',
  GET_WITH_ROLE_PERMISSION_URL: '/User/GetWithRolePermission',
  GET_ALL_WITH_ROLES_URL: '/UserRole/GetListUserRoles',

  GET_URL: '/User/Get',
  GET_INFO_URL: '/User/GetInfo',
  GET_DETAIL_URL: '/User/GetUserDetail',
  ADD_URL: '/User/Add',
  EDIT_URL: '/User/Edit',
  UPDATE_DETAIL_URL: '/User/UpdateDetail',
  DELETE_URL: '/User/Delete',
  CHANGE_PASSWORD_URL: '/User/ChangePassword'
}

const ROLE_URLS = {
  GET_LIST_ROLE_PERMISSIONS_URL: '/RolePermission/GetListRolePermissions',
  GET_ALL_URL: '/Role/GetAll',
  GET_URL: '/Role/Get',
  ADD_URL: '/Role/Add',
  EDIT_URL: '/Role/Edit',
  DELETE_URL: '/Role/Delete'
}
const PARCEL_URLS = {
  GET_ALL_URL: '/Parcel/GetAll',
  CHANGE_STATUS_URL: '/Parcel/ChangeStatus',
  SET_OPTIONS_URL: '/Parcel/SetOptions',
  GET_URL: '/Parcel/Get',
  ADD_URL: '/Parcel/Add',
  EDIT_URL: '/Parcel/Edit',
  DELETE_URL: '/Parcel/Delete'
}
const SELLER_URLS = {
  GET_ALL_URL: '/Seller/GetAll',
  GET_URL: '/Seller/Get',
  ADD_URL: '/Seller/Add',
  EDIT_URL: '/Seller/Edit',
  DELETE_URL: '/Seller/Delete'
}
const CATEGORY_URLS = {
  GET_ALL_URL: '/Category/GetAll',
  GET_URL: '/Category/Get',
  ADD_URL: '/Category/Add',
  EDIT_URL: '/Category/Edit',
  DELETE_URL: '/Category/Delete'
}
const CURRENCY_URLS = {
  GET_ALL_URL: '/Currency/GetAll',
  GET_URL: '/Currency/Get',
  ADD_URL: '/Currency/Add',
  EDIT_URL: '/Currency/Edit',
  DELETE_URL: '/Currency/Delete'
}
const STORE_URLS = {
  GET_ALL_URL: '/Store/GetAll',
  GET_URL: '/Store/Get',
  ADD_URL: '/Store/Add',
  EDIT_URL: '/Store/Edit',
  DELETE_URL: '/Store/Delete'
}

const PERMISSION_URLS = {
  GET_ALL_URL: '/Permission/GetAll',
  GET_ALL_DIRECTIVE_PERMISSIONS_URL: '/Permission/GetAllDirectivePermissions'
}

export default {
  AUTHORIZE_URLS,
  USER_URLS,
  SELLER_URLS,
  CATEGORY_URLS,
  CURRENCY_URLS,
  ROLE_URLS,
  PERMISSION_URLS,
  STORE_URLS,
  PARCEL_URLS
}
