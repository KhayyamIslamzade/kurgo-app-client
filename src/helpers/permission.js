export function getRolePermissions(roles) {
  let permissions = []
  roles.forEach(role => {
    permissions = permissions.concat(role.permissions)
  })
  return permissions
}
export function getRoleNames(roles) {
  return roles.map(c => c.name)
}

export function hasRole(roles, route) {
  if (isPermissionOrRoleMetaExist(route)) {
    if (isRoleMetaExist(route)) {
      let result = false
      if (roles && roles.length > 0) { result = roles.some(role => roles.includes(role.name)) }
      return result
    }

    return false
  } else {
    return true
  }
}
export function isPermissionOrRoleMetaExist(route) {
  return (
    isRoleMetaExist(route) ||
    isPermissionMetaExist(route) ||
    isDirectivePermissionMetaExist(route)
  )
}
export function isRoleMetaExist(route) {
  return route.meta && route.meta.roles && route.meta.roles.length > 0
}
export function isPermissionMetaExist(route) {
  return (
    route.meta && route.meta.permissions && route.meta.permissions.length > 0
  )
}
export function isDirectivePermissionMetaExist(route) {
  return (
    route.meta &&
    route.meta.directivePermissions &&
    route.meta.directivePermissions.length > 0
  )
}
export function hasPermission(permissions, route) {
  if (isPermissionOrRoleMetaExist(route)) {
    if (isPermissionMetaExist(route)) {
      let result = false
      if (permissions && permissions.length > 0) {
        result = permissions.some(permission =>
          route.meta.permissions.includes(permission)
        )
      }
      return result
    }

    return false
  } else {
    return true
  }
}
export function hasDirectivePermission(directivePermissions, route) {
  if (isPermissionOrRoleMetaExist(route)) {
    if (isDirectivePermissionMetaExist(route)) {
      let result = false
      if (directivePermissions && directivePermissions.length > 0) {
        result = directivePermissions.some(directivePermission =>
          route.meta.directivePermissions.includes(directivePermission)
        )
      }

      return result
    }

    return false
  } else {
    return true
  }
}
