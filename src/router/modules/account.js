/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const accountRouter = {
  path: '/account',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Account',
  meta: {
    title: 'Account',
    icon: 'people'
  },
  children: [
    {
      path: 'users',
      component: () => import('@/views/account/users'),
      name: 'Users',
      meta: {
        title: 'Users',
        noCache: true,
        permissions: ['user_list'],
        directivePermissions: ['moderator']
      }
    },
    {
      path: 'Roles',
      component: () => import('@/views/account/roles'),
      name: 'Roles',
      meta: { title: 'Roles', noCache: true, permissions: ['role_list'] }
    }
  ]
}

export default accountRouter
