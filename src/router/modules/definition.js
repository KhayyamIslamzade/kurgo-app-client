/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const definitionRouter = {
  path: '/definition',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Definition',
  meta: {
    title: 'Definition',
    icon: 'skill'
  },
  children: [
    {
      path: 'category',
      component: () => import('@/views/definition/category'),
      name: 'productCategories',
      meta: {
        title: 'Product Categories',
        noCache: true,
        permissions: ['category_list']
        // permissions: ["user_list"],
        // directivePermissions: ["moderator"]
      }
    },
    {
      path: 'seller',
      component: () => import('@/views/definition/seller'),
      name: 'seller',
      meta: { title: 'Sellers', noCache: true, permissions: ['seller_list'] }
    },
    {
      path: 'currency',
      component: () => import('@/views/definition/currency'),
      name: 'currency',
      meta: {
        title: 'Currencies',
        noCache: true,
        permissions: ['currency_list']
      }
    },
    {
      path: 'store',
      component: () => import('@/views/definition/store'),
      name: 'store',
      meta: { title: 'Stores', noCache: true, permissions: ['store_list'] }
    }
  ]
}

export default definitionRouter
